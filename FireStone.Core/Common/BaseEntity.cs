﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FireStone.Core.Common
{
   public class BaseEntity
    {
        public int Id { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string CreatedBy { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string ModifiedBy { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
    }

    public class Address
    {
        [Column(TypeName = "varchar(50)")]
        public string Address1 { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string Address2 { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string City { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string County { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string State { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string PostalCode { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string Country { get; set; }
    }

    public enum MpesaServices
    {
        C2B=1,
        B2C=2,
        B2B=3
    }


}
