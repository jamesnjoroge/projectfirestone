﻿using FireStone.Core.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace FireStone.Core.Registration
{
   public partial class Company : BaseEntity
    {
        [Column(TypeName = "varchar(5)")]
        public string CompanyCode { get; set; }
        [Column(TypeName = "varchar(5)")]
        public string CompanyName { get; set; }
        public Address Address { get; set; }

    }

    // this are the services that a company or users can subscribe to.
    // this will include but not limited to mpesa(Wallet),Bank,Alerts,USSD,PesaLink
    public partial class Services : BaseEntity
    {
        // 5 characters alphanumeric code
        [Column(TypeName = "varchar(5)")]
        public string Code { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string Name { get; set; }
        [Column(TypeName = "varchar(150)")]
        public string Description { get; set; }
    }
    //public int MyProperty { get; set; }
    public class MpesaServiceSetup : BaseEntity
    {
       // use this to setup mpesa credential and urls
       // this will sort for both api and the saas.
       // 
        public string  CompanyCode { get; set; }
        public string ServiceCode { get; set; }
        public MpesaServices ServicesType { get; set; }
        public string PaybillNumber { get; set; }
        public string Url { get; set; }
        public string CallbackUrl { get; set; }
        public string ApiKey { get; set; }
        public string Secret { get; set; }
        public string ApiToken { get; set; }
        public decimal Balance { get; set; }
        public decimal ThresholdLimit { get; set; }


    }

    // use this to setup alerts services
    public class SmsProviders: BaseEntity
    {
        [Column(TypeName = "varchar(5)")]
        public string Code { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string Name { get; set; }

        [Column(TypeName = "varchar(100)")]
        public string BaseUrl { get; set; }
        [Column(TypeName = "varchar(100)")]
        public string SmsUrl { get; set; }
        [Column(TypeName = "varchar(100)")]
        public string BulkSmsUrl { get; set; }

    }
    public class SmsServiceSetUp : BaseEntity
    {
        // Here We will setup the sms alerts for our client
        // we will link this to a company and a selected provider above.
        // we will also get a phone number to send an alert when a threshold is met.
        public string CompanyCode { get; set; }
        public string SmsProviderCode { get; set; }
        public decimal Balance { get; set; }
        public decimal alertThreshold { get; set; }
        public string AlertPhoneNumber { get; set; }
        public string AlertEmail { get; set; }
        public bool Alert { get; set; }
    }
    public class MpesaSTKPush : BaseEntity
    {


    }
}
